// IBetaAppUpdates.aidl
package com.clearos.updates;

// Declare any non-default types here with import statements
import com.clearos.updates.IAppUpdateCallback;

interface IBetaAppUpdates {
    /**
     * Starts updating the specified version of a package for installation. Download progress callbacks
     * will be triggered if a callback has been registered.
     */
    oneway void startAppUpdate(String packageName, long versionCode);
    /**
     * Gets the latest version/update info of the app with given package name.
     */
    oneway void getUpdateInfo(String packageName, long versionCode);
    /**
     * Register a callback interface for in-app updates.
     */
    oneway void registerCallback(IAppUpdateCallback callback);
    /**
     * Unregister a callback interface for in-app updates.
     */
    oneway void uregisterCallback(IAppUpdateCallback callback);
    /**
     * Ask the service to restart the app after a given time.
     */
    oneway void restartApp(String packageName, int delaySeconds);
}
