package com.clearos.updates;

import android.os.RemoteException;

public interface IUpdateHandler {
    void onUpdateCheck(UpdateInfo info);
    void onDownloadProgress(float progress);
    void onInstallStarted();
    void onInstallCompleted(String version);
    void onInstallFailed(String errorMessage);
    void onRemoteError(RemoteException e);
}
