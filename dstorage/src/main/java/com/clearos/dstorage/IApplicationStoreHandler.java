package com.clearos.dstorage;

import java.io.File;
import java.io.IOException;

public interface IApplicationStoreHandler {
    void onFileSaved(File localFile);
    void onIOError(IOException e);
    void onFileLoaded(File localFile);
}
