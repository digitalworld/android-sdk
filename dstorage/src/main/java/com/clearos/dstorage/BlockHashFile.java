package com.clearos.dstorage;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import java.io.IOException;

import io.ipfs.multibase.Base58;
import io.ipfs.multihash.Multihash;

public class BlockHashFile implements Parcelable {
    public Multihash cid;
    public String[] dag;
    public long size;
    byte[] indexCipher;
    private boolean isDirectory;

    private final static String TAG = "BlockHashFile";

    public BlockHashFile(Multihash _cid, String[] _dag, long _size, byte[] _indexCipher) {
        cid = _cid;
        dag = _dag;
        size = _size;
        indexCipher = _indexCipher;
        isDirectory = false;
    }

    public BlockHashFile(Multihash _cid, String[] _dag, long _size, byte[] _indexCipher, boolean isDirectory) {
        cid = _cid;
        dag = _dag;
        size = _size;
        indexCipher = _indexCipher;
        this.isDirectory = isDirectory;
    }

    public String getCid() {
        return cid.toBase58();
    }

    public BlockHashFile(Parcel in) {
        try {
            String cidBase58 = in.readString();
            if (cidBase58 != null)
                cid = Multihash.deserialize(Base58.decode(cidBase58));
        } catch (IOException e) {
            Log.e(TAG, "Error deserializing Base58 CID from Parcel.", e);
        }
        dag = new String[in.readInt()];
        in.readStringArray(dag);
        size = in.readLong();
        isDirectory = in.readBoolean();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(cid.toBase58());
        dest.writeInt(dag.length);
        dest.writeStringArray(dag);
        dest.writeLong(size);
        dest.writeBoolean(isDirectory);
    }

    public static final Parcelable.Creator<BlockHashFile> CREATOR
            = new Parcelable.Creator<BlockHashFile>() {
        public BlockHashFile createFromParcel(Parcel in) {
            return new BlockHashFile(in);
        }

        public BlockHashFile[] newArray(int size) {
            return new BlockHashFile[size];
        }
    };
}
