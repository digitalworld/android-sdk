package com.clearos.dstorage;

import android.content.Context;
import android.content.Intent;
import android.content.res.XmlResourceParser;
import android.net.Uri;
import android.os.Build;
import android.util.Log;
import android.util.NoSuchPropertyException;
import android.util.Pair;

import com.amazonaws.util.IOUtils;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.File;
import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static androidx.core.content.FileProvider.getUriForFile;

public class ApplicationStore {
    private final DecentralizedStoreClient client;
    private final Context appContext;
    private IApplicationStoreHandler handler;
    private IApplicationStoreHandler oldHandler = null;
    private final String fileProviderName;
    private final String sharedDirectory;

    private final static String TAG = "DSFileProvider";
    private static Map<String, String> configuredPaths = null;

    /**
     * Creates a class to handle sharing of files with ClearCLOUD to have them put in decentralized
     * storage.
     * @param client Decentralized storage client for the ClearCLOUD storage service configured by
     *               the calling application.
     * @param fileProviderName Name of the file provider configured in the application manifest.
     * @param sharedDirectory name of the shared directory in XML file_paths.
     * @throws NoSuchPropertyException if the specified sharedDirectory is not configured for the application.
     */
    public ApplicationStore(DecentralizedStoreClient client, String fileProviderName, String sharedDirectory, IApplicationStoreHandler handler) {
        this.client = client;
        appContext = client.getAppContext();
        this.handler = handler;
        this.sharedDirectory = sharedDirectory;
        if (sharedDirectory == null)
            Log.w(TAG, "Shared directory is null; this will not work.");

        this.fileProviderName = fileProviderName;

        File localDir = getLocalDirectory();
        if (localDir != null && !localDir.exists()) {
            Log.i(TAG, "Creating new local directory for application store: " + sharedDirectory);
            try {
                Files.createDirectories(localDir.toPath());
            } catch (IOException e) {
                handler.onIOError(e);
            }
        } else
            Log.w(TAG, "Couldn't get a local directory at " + sharedDirectory);
    }

    /**
     * Replaces the handler for application storage events.
     */
    public void replaceHandler(IApplicationStoreHandler newHandler) {
        oldHandler = handler;
        handler = newHandler;
    }

    /**
     * Restores the application storage event handler to what it was before `replaceHandler` was
     * called.
     */
    public void restoreHandler() {
        handler = oldHandler;
        oldHandler = null;
    }

    /**
     * Sets a view model to append results to as progress is mode on an operation.
     */
    public void setViewModel(ProgressViewModel progressViewModel) {
        client.setViewModel(progressViewModel);
    }
    public ProgressViewModel getViewModel() {
        return client.getViewModel();
    }

    /**
     * Gets a map of configured paths and their names in the resources XML file.
     */
    public static Map<String, String> getConfiguredFilePaths(Context appContext, int xmlResourceId) throws XmlPullParserException, IOException {
        if (configuredPaths != null) {
            return configuredPaths;
        }

        XmlResourceParser parser = appContext.getResources().getXml(xmlResourceId);
        configuredPaths = new HashMap<>();

        int eventType = parser.getEventType();
        while (eventType != XmlPullParser.END_DOCUMENT) {
            if (eventType == XmlPullParser.START_TAG) {
                String name = parser.getName();
                if (name.equals("files-path")) {
                    Log.d(TAG, String.format("Configuring file path from XML with %s and %s",
                            parser.getAttributeValue(null, "name"),
                            parser.getAttributeValue(null, "path")
                            ));
                    configuredPaths.put(parser.getAttributeValue(null, "name"),
                            parser.getAttributeValue(null, "path"));
                }
            }
            eventType = parser.next();
        }

        parser.close();
        return configuredPaths;
    }

    /**
     * Saves the specified files to decentralized storage via a request to the ClearCLOUD system.
     * @param targets Pairs of labels/displayNames and the actual Files that they point to. If the label
     *                is null it will be inferred from the file name.
     * @return Keys are the file objects passed in as `targets`; values are bool and indicate whether
     *         the file was actually queued for decentralized storage. Files that do not exist locally
     *         are not queued.
     */
    @SafeVarargs
    public final Map<File, Boolean> save(Pair<String, File>... targets) {
        Map<File, Boolean> result = new HashMap<>();
        List<InteroperableFile> requestFiles = new ArrayList<>();

        for (Pair<String, File> p: targets) {
            // Extract the label and file from each pair.
            File file = p.second;
            String label = p.first;
            if (label == null) {
                label = file.getName();
            }
            // Note that this fileName is for interoperability purposes. The decentralized storage
            // uses this string to keep track of a package's files. It isn't the file name in a
            // file system sense, rather like a file identifier.
            String fileName = String.format("%s/%s", sharedDirectory, file.getName());

            if (!file.exists()) {
                Log.d(TAG, String.format("File %s does not exist; cannot add to storage", file.getAbsolutePath()));
                result.put(file, false);
            } else {
                // Create a content URI from the file provider and grant ClearCLOUD permission to read it.
                Uri contentUri = getUriForFile(appContext, fileProviderName, file);
                String SHARE_TARGET = "com.clearos.clearlife";

                if (!Build.MODEL.contains("ClearPHONE") && !Build.MODEL.contains("FreedomPhone"))
                    SHARE_TARGET = "com.clearos.digitallife";

                appContext.grantUriPermission(SHARE_TARGET, contentUri, Intent.FLAG_GRANT_READ_URI_PERMISSION);

                InteroperableFile iFile = new InteroperableFile(file, contentUri, label, fileName);
                requestFiles.add(iFile);
                result.put(file, true);
            }
        }

        if (requestFiles.size() > 0) {
            for (InteroperableFile iFile: requestFiles) {
                client.storeFile(iFile.getFileName(), iFile.getLabel(), iFile.getUri(), fileName -> {
                    handler.onFileSaved(iFile.getFile());
                    return null;
                }, null, null);
            }
        }

        return result;
    }

    /**
     * Gets a reference to a file within the configured store directory.
     * @param fileName Name of the file to get.
     * @return Null if the directory has no configured path.
     */
    public File getFile(String fileName) {
        File localDir = getLocalDirectory();
        if (localDir != null) {
            return new File(localDir, fileName);
        } else {
            return null;
        }
    }

    /**
     * Returns a File object pointing to the actual directory in the file system.
     */
    public File getLocalDirectory() {
        String configuredPath = configuredPaths.getOrDefault(sharedDirectory, null);
        if (configuredPath != null) {
            return new File(appContext.getFilesDir(), configuredPath);
        } else {
            return null;
        }
    }

    /**
     * Saves a file descriptor to a local file.
     * @param name Name of the file that is being retrieved from storage.
     * @param descriptor Descriptor shared from ClearCLOUD.
     */
    private File saveFile(String name, FileDescriptor descriptor) {
        File local = new File(getLocalDirectory(), name);
        try {
            FileOutputStream fos = new FileOutputStream(local);
            FileInputStream fis = new FileInputStream(descriptor);
            IOUtils.copy(fis, fos);
        } catch (IOException e) {
            handler.onIOError(e);
        }

        return local;
    }

    /**
     * Saves the specified files to decentralized storage via a request to the ClearCLOUD system.
     * @param targets File names to retrieve from the decentralized store; these are the same as the
     *                `file.getName()` with which they were stored (in the context of the shared
     *                directory).
     * @return Keys are the file names passed in as `targets`; values are bool and indicate whether
     *         the file was actually queued for download.
     */
    public Map<String, Boolean> load(String... targets) throws IOException {
        Map<String, Boolean> result = new HashMap<>();
        List<String> requestFiles = new ArrayList<>();
        File localDirectory = getLocalDirectory();

        for (String name: targets) {
            // Make sure that the file doesn't already exist locally.
            File targetFile = new File(localDirectory, name);

            if (targetFile.exists()) {
                result.put(name, false);
            } else {
                // Note that this fileName is for interoperability purposes. The decentralized storage
                // uses this string to keep track of a package's files. It isn't the file name in a
                // file system sense, rather like a file identifier.
                String fileName = String.format("%s/%s", sharedDirectory, name);
                requestFiles.add(fileName);
            }
        }

        if (requestFiles.size() > 0) {
            for (String name: requestFiles) {
                client.loadFile(name, descriptor -> {
                    File local = saveFile(name, descriptor);
                    handler.onFileLoaded(local);
                    return null;
                }, null, null);
            }
        }

        return result;
    }
}

