package com.clearos.dlt;

import android.os.Parcel;
import android.os.Parcelable;

import com.goterl.lazysodium.utils.Key;
import com.goterl.lazysodium.utils.KeyPair;

import java.util.Arrays;

public class CryptoKeyPair implements Parcelable {
    private KeyPair keys;

    public Key getPublicKey() {
        return keys.getPublicKey();
    }

    public Key getSecretKey() {
        return keys.getSecretKey();
    }

    public CryptoKeyPair(KeyPair keys) {
        this.keys = keys;
    }

    /**
     * Reconstructs a DidKeys instance from a parcel.
     * @param in Parcel from the AIDL service.
     */
    public CryptoKeyPair(Parcel in) {
        // Reconstruct the public and private key pairs.
        int pkLen = in.readInt();
        int skLen = in.readInt();
        byte[] pk = new byte[pkLen];
        in.readByteArray(pk);
        byte[] sk = new byte[skLen];
        in.readByteArray(sk);
        Key publicKey = Key.fromBytes(pk);
        Key secretKey = Key.fromBytes(sk);
        keys = new KeyPair(publicKey, secretKey);
    }

    public KeyPair getKeys() {
        return keys;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        byte[] pk = keys.getPublicKey().getAsBytes();
        byte[] sk = keys.getSecretKey().getAsBytes().clone();
        dest.writeInt(pk.length);
        dest.writeInt(sk.length);
        dest.writeByteArray(pk);
        dest.writeByteArray(sk);
        Arrays.fill(sk, (byte)0);
    }

    public static final Parcelable.Creator<CryptoKeyPair> CREATOR
            = new Parcelable.Creator<CryptoKeyPair>() {
        public CryptoKeyPair createFromParcel(Parcel in) {
            return new CryptoKeyPair(in);
        }

        public CryptoKeyPair[] newArray(int size) {
            return new CryptoKeyPair[size];
        }
    };
}
