package com.clearos.identity;

public class MqttUnsubscribeEvent {
    private String packageName;
    private String topic;

    /**
     * Event that represents unsubscription from *all* topics.
     * @param packageName The package to unsubscribe all topics.
     */
    public MqttUnsubscribeEvent(String packageName) {
        this.packageName = packageName;
        this.topic = null;
    }

    /**
     * Unsubscribes a specific topic for the given package.
     */
    public MqttUnsubscribeEvent(String packageName, String topic) {
        this.packageName = packageName;
        this.topic = null;
    }

    public String getPackageName() {
        return packageName;
    }

    public String getTopic() {
        return topic;
    }
}
