package com.clearos.identity;

import android.app.Application;
import android.content.pm.PackageManager;
import android.os.Handler;
import android.os.Looper;
import android.os.RemoteException;
import android.util.Log;
import android.widget.Toast;

import com.clearos.dlt.DecryptedMqttMessage;
import com.clearos.dlt.DidKeys;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;

public class KeyClientApplication extends Application {
    private static KeyClientApplication mInstance;
    private final String TAG = "KeyClientApplication";

    private final KeyClient client = new KeyClient();

    /**
     * Removes all registration callbacks from the application instance.
     */
    public void clearRegistrationCallbacks(String packageName) {
        client.clearRegistrationCallbacks(packageName);
    }

    /**
     * Adds a function to call once the app has successfully registered.
     */
    public void addRegistrationCallback(String packageName, Function<KeyClient, Void> callback) {
       client.addRegistrationCallback(packageName, callback);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        client.create();
        mInstance = this;
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
        client.terminate();
    }

    public void threadSafeToast(final String message) {
        new Handler(Looper.getMainLooper()).post(() -> Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show());
    }

    public DidKeys getAppKeys(String packageName) {
        return client.getAppKeys(packageName);
    }

    public DidKeys getAppKeys() {
        return getAppKeys(getPackageName());
    }

    /**
     * Checks whether the package with specified name is installed.
     * @param packageName FQN of the package to check.
     * @return True if the package is installed.
     */
    public boolean isPackageInstalled(String packageName) {
        return client.isPackageInstalled(this, packageName);
    }

    /**
     * Check is ClearLIFE is installed.
     * @return True if ClearLIFE is installed; otherwise false.
     */
    public boolean isClearLifeInstalled() {
        return client.isClearLifeInstalled(this);
    }

    public void setupKeys(String packageName, Function<DidKeys, Void> callback, Function<DecryptedMqttMessage, Void> notificationCallback, boolean isDemo) {
        client.setupKeys(this, packageName, callback, notificationCallback, isDemo);
    }

    /**
     * Returns one of the extra key clients for an arbitrary package. Note that only ClearLIFE
     * will have non-null custom package derived key clients. Other apps should expect unhandled
     * exceptions.
     * @return Null if it hasn't been initialized.
     */
    public DerivedKeyClient getKeyClient(String packageName) {
        return client.getKeyClient(packageName);
    }

    public DerivedKeyClient getKeyClient() {
        Log.d(TAG, "Returning key client for default application name " + getPackageName());
        return getKeyClient(getPackageName());
    }

    //To create instance of base class
    public static synchronized KeyClientApplication getInstance() {
        return mInstance;
    }

}
