package com.clearos.identity;

public class NotificationTopicSubscription {
    private final String appDid;
    private final String topic;

    public NotificationTopicSubscription(String appDid, String topic) {
        this.appDid = appDid;
        this.topic = topic;
    }

    public String getAppDid() {
        return appDid;
    }

    public String getTopic() {
        return topic;
    }


}
