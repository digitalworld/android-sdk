package com.clearos.identity;

import android.os.RemoteException;
import android.util.Log;

import com.clearos.dlt.DecryptedMqttMessage;

import java.io.IOException;

public class LoggingRemoteErrorHandler implements IRemoteErrorHandler {
    private final String TAG = "LoggingRemoteErrorHandler";

    @Override
    public void onGenericError(Exception e) {
        Log.e(TAG, "Generic error from derived key client children.", e);
    }
    @Override
    public void onRemoteError(RemoteException e) {
        Log.e(TAG, "Remote error in derived key client from document provider.", e);
    }
    @Override
    public void onIOError(IOException e) {
        Log.e(TAG, "I/O error in decentralized storage from document provider.", e);
    }

    @Override
    public void onNotificationReceived(DecryptedMqttMessage message) {
        Log.i(TAG, "Push notification received from ClearDID.");
    }
}