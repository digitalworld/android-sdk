package com.clearos.identity;

public enum ClearPackages {
    gm("com.clearos.cleargm"),
    life("com.clearos.clearlife"),
    id("com.clearid"),
    pay("com.clearos.clearpay"),
    digitallife("com.clearos.digitallife");

    private final String val;
    private ClearPackages(String v) { val = v; }
    public String getVal() { return val; }
}
